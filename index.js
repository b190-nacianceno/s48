let posts = [];

let count = 1;


// adding post data

document.querySelector(`#form-add-post`).addEventListener(`submit`,(e)=> {
    // prevents the page from reloading after a button has been clicked/triggered
    e.preventDefault();

    // adds the information from the "frontend" to our mock database
    posts.push({
        id:count,
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    });

    // Starts at 1 
    count++;

    showPosts(posts);
    alert('Successfully Added.');
});


// showPosts();
const showPosts = (posts) => {
	let postEntries = '';

    // we used forEach method so that each of the JSON inside our mock database will be rendered inside the #div-post-entries div
	posts.forEach((post)=>{
        // in javascript it is also possible to render html elements with the use of functions/methods plus appropriate syntax such as template literals (backticks plus ${});
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title.value}</h3>
				<p id="post-body-${post.id}">${post.body.value}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	})
    // the div duplicates the objects inside the postEntries variable
	document.querySelector('#div-post-entries').innerHTML = postEntries;


}


// create an editPost function that lets us transfer the id, title and bod of each post entry to the second form 


const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;


    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
    
}

// Update post

document.querySelector('#form-edit-post').addEventListener('submit',(e) => {
    e.preventDefault();

    for(let i = 0; i < posts.length; i++){
        // the value of posts[i].id is a Number but the value for document.querySelector('#txt-edit-id').value is a String
        //  it is necessary to conver the Number into String first before comparing the two values
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
            posts[i].title = document.querySelector('#txt-edit-title').value;
            posts[i].body = document.querySelector('#txt-edit-body').value;

            showPosts(posts);
            alert('Successfully updated');

            break;
        }
    }
})

// Delete post 